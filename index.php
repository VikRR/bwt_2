<?php

define('ROOT',dirname(__FILE__));

require_once ROOT . '/core/Router.php';
require_once ROOT . '/core/Controller.php';
require_once ROOT . '/core/Model.php';
require_once ROOT . '/core/View.php';
require_once ROOT . '/core/Sessions.php';
require_once ROOT . '/vendor/autoload.php';
session_start();
$router = new Router();
$router -> run();
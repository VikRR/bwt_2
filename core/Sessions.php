<?php

/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 03.01.17
 * Time: 19:27
 */
class Sessions
{
  /**
   * @param string $param1
   * @param string $param2
   */
  static function start($param1, $param2){
    isset($_SESSION['user'])? session_destroy() : session_start();
    $_SESSION['user'] = $param1;
    $_SESSION['userid'] = $param2;
  }

  /**
   *
   */
  static function stop(){
    if(isset($_SESSION['user'])){
      session_destroy();
    }
  }
}
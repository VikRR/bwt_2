<?php

/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 30.12.16
 * Time: 15:43
 */
class Controller
{
public $model;
public $view;

  /**
   * Controller constructor.
   */
  public function __construct()
  {
    $this->view = new View();
  }

  /**
   * @param string $model
   * @return object mixed
   */
  public function loadModel($model){
    $model = ucfirst($model);
    $model_path = ROOT.'/models/' . $model . '_model.php';
    if(file_exists($model_path)){
      require_once ROOT.'/models/' . $model . '_model.php';
      $model_name = $model . '_model';
      return $this->model = new $model_name();
    }
  }

}
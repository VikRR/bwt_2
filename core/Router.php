<?php

/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 28.12.16
 * Time: 17:29
 */
class Router
{
  private $routes;

  /**
   * Router constructor.
   */
  public function __construct()
  {
    $routes_path = 'config/routes.php';
    $this -> routes = require_once $routes_path;
  }

  /**
   * Return request string
   */
  public function getURL(){
    if (!empty($_SERVER['REQUEST_URI'])){
      return parse_url(ltrim($_SERVER['REQUEST_URI'], '/'), PHP_URL_PATH);
    }
  }

  /**
   *Run router
   */
  public function run(){

    $url = $this -> getURL();

    if (empty($url)){
      require_once ROOT.'/controllers/Index.php';
      $controller_obj = new Index();
      $controller_obj->index();
    }
    foreach ($this->routes as $url_pattern => $path){
      if (preg_match("~$url_pattern~", $url)){
        $internal_route = preg_replace("~$url_pattern~", $path, $url);

        $array_controller_action = explode('/',$internal_route);

        $controller_name = ucfirst(array_shift($array_controller_action));

        $method_name = array_shift($array_controller_action);

        $param = $array_controller_action;

        $controller_file = ROOT.'/controllers/' . $controller_name . '.php';

        if (file_exists($controller_file)){
          include_once $controller_file;
        }

        $controller_obj = new $controller_name();

        $res = call_user_func_array(array($controller_obj, $method_name), $param);

        if ($res != null){
          break;
        }else{
          echo '<br>Error 404. Page not found.<br>';
        }
      }
    }
  }
}
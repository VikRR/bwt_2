<?php

/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 29.12.16
 * Time: 12:23
 */
class Model
{
  private $database;
  public $conf_db = array();
  /**
   * Model constructor.
   */
  public function __construct(){
    $database_path = 'config/database.php';
    $this->database = require_once 'config/database.php';
    $this->database_option = require_once 'config/db_options.php';
  }


  /**
   * @return bool|PDO
   */
  public function database(){
    foreach ($this->database as $k => $v){
      $this ->conf_db[$k] = $v;
    }
    $host = $this->conf_db['host'];
    $db = $this->conf_db['db'];
    $dns = "mysql:host = $host; dbname=$db; charset=utf8;";
    try{
      return $db = new PDO($dns, $this->conf_db['user'], $this->conf_db['password'], $this->database_option);
    }catch(PDOException $e){
      echo 'Connection failed: ' . $e->getMessage();
      return false;
    }
  }

  /**
   * @param string $table
   * @param null $id
   * @return array|bool
   */
  public function getAll($table, $id=null){
    $ps = $this -> database();
    try{
      if(!is_null($id)){
        $sel = $ps -> prepare("select * from {$table} where id={$id}");
      }else{
        $sel = $ps -> prepare("select * from {$table}");
      }
      $sel -> execute();
      while($row = $sel -> fetch()){
        $result_array[] = $row;
      }
      return $result_array;
    }catch(PDOException $e){
      echo '<br><strong>Select failed: ' . $e -> getMessage() . '</strong>';
      return false;
    }
  }

}
<?php

/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 30.12.16
 * Time: 15:44
 */
class View
{

  /**
   * View constructor.
   */
  public function __construct()
  {

  }

  /**
   * @param string $view
   * @param array null $data
   */
  public function load($view, $data=null){
    if(isset($_SESSION['user'])){
      require_once ROOT.'/views/header_view.php';
      require_once ROOT.'/views/nav_autorized_view.php';
      require_once ROOT.'/views/' . $view . '_view.php';
      require_once ROOT.'/views/footer_view.php';
    }else{
      require_once ROOT.'/views/header_view.php';
      require_once ROOT.'/views/' . $view . '_view.php';
      require_once ROOT.'/views/footer_view.php';
    }
  }

  /**
   * @param string $method
   * @return array
   */
  public function input($method){
    $data = array();
    if($method == 'post'){
      foreach($_POST as $k => $v){
        $data[$k] = $v;
      }
    }else{
      foreach($_GET as $k => $v){
        $data[$k] = $v;
      }
    }
    return $data;
  }

}
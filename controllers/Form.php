<?php

/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 28.12.16
 * Time: 18:44
 */

class Form extends Controller
{
  private $errors = array();

  /**
   * Form constructor.
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * @param array $row
   * @param string $err
   */
  public function unique($row, $err)
  {
    array_shift($row) == 0 ?: $this->errors[] = $err;
  }

  /**
   * @param string $param1
   * @param string $param2
   * @param string $err
   */
  public function equality($param1, $param2, $err)
  {
    $param1 == $param2 ?: $this->errors[] = $err;
  }

  /**
   * @return array errors
   */
  public function getErrors()
  {
    return $this->errors;
  }

  /**
   * @param $array
   * @return array mixed
   */
  public function adaptation($array)
  {
    foreach ($array as $k => $v) {
      trim($array[$k] = $v);
    }
    return $array;
  }


  /**
   *form registration
   */
  public function registration()
  {
    $this->errors = array();

    if (!isset($_POST['reg'])) {

      $this->view->load('index/index');

    } else {
      $this->loadModel('form');

      $reg = $this->view->input('post');

      foreach($reg as $k => $v){
        if(empty($v)){
          $this -> errors["Empty field $k"];
          echo "Empty field $k";
          exit();
        }
      }
      $reg = $this->adaptation($reg);

      print_r($reg);

      $this->equality($reg['password'], $reg['repeat_password'], 'Passwords not match.');

      $email = $this->model->emailVal($reg['email']);

      $this->unique($email,'Email not unique.');

      $nameVal = $this->model->nameVal($reg['name'],$reg['last_name']);

      $this->unique($nameVal,'Name and last name already have an account with another email.');

      if (!empty($this->getErrors())) {
        foreach ($this->getErrors() as $v) {
          echo "<br> Errors: $v";
          exit();
        }
      }

      foreach ($reg as $k => $v) {
        if ($k != 'repeat_password') {
          if ($k != 'reg') {
            if ($k == 'password') {
              $data[] = password_hash($v, PASSWORD_DEFAULT);
            } else {
              $data[] = $v;
            }
          }
        }
      }
      $data[] = time();

      $this->model->registration($data);

      header('Location: login');
    }
  }


  /**
   * form login
   */
  public function login()
  {
    $this->loadModel('form');

    $this->errors = array();

    if(!isset($_POST['log'])){
      $this->view->load('form/login');
    }else{
      $data = $this->view->input('post');

      $data = $this->adaptation($data);

      $log = $this->model->emailVal($data['email']);

      !empty($log)?:$this->errors[] = 'Email incorrect, please enter other email.';

      password_verify($data['password'],$log['password']) ? : $this -> errors[] = 'Password incorrect.' ;

      if (!empty($this->getErrors())) {
        foreach ($this->getErrors() as $v) {
          echo "<br><strong>$v</strong>";
          exit();
        }
      }
      $username = $log['name'] . ' ' . $log['last_name'];

      $session = new Sessions();

      $session::start($username,$log['id']);

      header('Location:comment');
    }
  }

  /**
   *logout
   */
  public function logout(){
    $session = new Sessions();
    $session::stop();
    header('Location: /');
    exit();
  }
}

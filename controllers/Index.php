<?php

/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 30.12.16
 * Time: 21:09
 */
class Index extends Controller
{

  /**
   * Index constructor.
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   *
   */
  public function index(){
    if(isset($_SESSION['user'])){
      header('Location: weather');
    }else{
      $this -> view -> load('index/index');
    }
  }
}
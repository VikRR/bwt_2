<?php

/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 28.12.16
 * Time: 18:50
 */

class Comment extends Controller
{

  /**
   * Comment constructor.
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * @return bool
   */
  public function setComment(){
    if(!isset($_POST['add_comment'])){
      $this->view->load('comments/comments');
    }else{
      $this->loadModel('comment');
      $ins = $this->view->input('post');
      if($_SESSION['num'] == md5($ins['captcha'])){
        $data[] = $_SESSION['userid'];
        $data[] = $ins['comment'];
        $data[] = time();
        $this->model->setComment($data);
        header('Location: feedback');
      }else{
        echo 'Captcha not match.';
      }
    }
    return true;
  }


  /**
   * @return bool
   */
  public function showAllComments(){
    $this->loadModel('comment');
    $data = $this->model -> getAll('comments');
    $user = $this->model->getAll('users');
    foreach($data as $key => $val) {
      $data[$key] = $val;
      foreach ($user as $v) {
        if($data[$key]['user_id'] == $v['id']){
          $user_name = $v['name'] . ' ' . $v['last_name'];
          $data[$key]['user_name'] = $user_name;
          $data[$key]['user_city'] = $v['city'];
          $data[$key]['user_sex'] = $v['sex'];
        }
      }
    }
    $this->view->load('comments/feedback', $data);
    return true;
  }
}
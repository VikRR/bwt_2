<?php

/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 28.12.16
 * Time: 18:50
 */

class Weather extends Controller
{


  /**
   * Weather constructor.
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * @return bool
   */
  public function showWeather(){
    $this->loadModel('weather');
    $data=$this->model->getWeather();
    $weather_obj = json_decode($data);
    foreach($weather_obj as $k => $v){
      $weather_array[$k] = $v;
    }
    $this->view->load('weather/weather', $weather_array);
    return true;
  }
}
<?php
return array(
    'registration'              => 'form/registration',         // controller Form action registration
    'login'                     => 'form/login',                // controller Form action login
    'logout'                    => 'form/logout',               // controller Form action logout
    'comment'                   => 'comment/setComment',        // controller form action setComment
    'feedback'                  => 'comment/showAllComments',      // controller comment action showComments
    'weather'                   => 'weather/showWeather'        // controller weather action showWeather
);
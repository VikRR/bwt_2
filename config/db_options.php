<?php
/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 10.01.17
 * Time: 13:44
 */
return array (
  PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
  PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
  PDO::MYSQL_ATTR_INIT_COMMAND => 'set names utf8'
);
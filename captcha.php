<?php
session_start();

$num = rand(1000, 9999);
$_SESSION['num'] = md5($num);

$img = imagecreatetruecolor(130, 40);

$white = imagecolorallocate($img, 255, 255, 255);
$grey = imagecolorallocate($img, 128, 128, 128);
$black = imagecolorallocate($img, 0, 0, 0);

imagefilledrectangle($img, 0, 0, 200, 35, $black);

$font = 'fonts/Julee-Regular.ttf';
$size = 12;

imagettftext($img, 35, 0, 22, 24, $grey, $font, $num);

imagettftext($img, 35, 0, 15, 26, $white, $font, $num);

header("Expires: Wed, 1 Jan 1997 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

header("Content-type: image/gif");
imagegif($img);
imagedestroy($img);

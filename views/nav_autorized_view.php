<div class="row">
  <div class="col-md-12 text-center hello">
    <h3><?= 'Hello, ' . $_SESSION['user'];?></h3>
    <a href="logout"> Logout </a>
  </div>
  <div class="col-md-12 text-center">
    <nav>
      <ul class="nav navbar-nav menu">
        <li><a href="weather">Weather</a></li>
        <li><a href="comment">Add Comment</a></li>
        <li><a href="feedback">Feedback</a></li>
      </ul>
    </nav>
  </div>
</div>

<div class="row">
  <div class="col-sm-4 col-sm-offset-4 form-box">
    <div class="form-top text-center">
      <h3>Register</h3>
    </div><!-- form-top -->
    <div class="form-bottom contact-form">
      <form role="form" action="registration" method="post">
        <div class="form-group">
          <label class="sr-only" for="name">Name</label>
          <input type="text" name="name" placeholder="Name..." class="form-control">
        </div><!-- form-group -->
        <div class="form-group">
          <label class="sr-only" for="last name">Last Name</label>
          <input type="text" name="last name" placeholder="Last Name..." class="form-control">
        </div><!-- form-group -->
        <div class="form-group">
          <label class="sr-only" for="email">Email</label>
          <input type="email" name="email" class="form-control" placeholder="Email">
        </div><!-- form-group -->
        <div class="form-group">
          <label class="sr-only" for="password">Password</label>
          <input type="password" name="password" placeholder="Password..." class="form-control">
        </div><!-- form-group -->
        <div class="form-group">
          <label class="sr-only" for="repeat password">Confirm Password</label>
          <input type="password" name="repeat password" placeholder="Repeat Password..." class="form-control">
        </div><!-- form-group -->
        <div class="form-group">
          <label class="sr-only" for="sex">Sex</label>
          <input type="text" name="sex" placeholder="Male or Female..." class="form-control">
        </div><!-- form-group -->
        <div class="form-group">
          <label class="sr-only" for="birthday">Birthday</label>
          <input type="text" name="birthday" placeholder="Birthday M/D/Y" class="form-control">
        </div><!-- form-group -->
        <div class="form-group">
          <label class="sr-only" for="city">City</label>
          <input type="text" name="city" placeholder="City" class="form-control">
        </div><!-- form-group -->
        <div class="text-center">
          <input type="submit" name="reg" class="btn btn-success" value="Go, registration!">
        </div>
      </form>
      <div class="form-top-sign text-center">
        <h4>Already have an account?</h4>
        <hr>
      </div><!-- form-top-sign -->
      <div class="text-center">
        <a class="btn btn-default" href="login">Sign In</a>
      </div>
    </div><!-- form-bottom contact-form -->
  </div><!-- col-sm-4 col-sm-offset-4 form-box -->
</div><!-- row -->


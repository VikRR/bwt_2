<div class="row feedback">
  <?php foreach($data as $k => $v):?>
    <div class="row block">
      <?php if($k%2 == 0 ):?>
        <div class="col-md-2 user">
          <p><?php echo $v['user_name'];?></p>
          <p><?php echo $v['user_city'];?></p>
        </div>
        <div class="col-md-10 comment">
          <p><?php echo date('l jS \of F Y h:i:s A', $v['date_comment']);?></p>
          <p><?php echo $v['comment'];?></p>
        </div>
      <?php else:?>
        <div class="col-md-10 comment">
          <p><?php echo date('l jS \of F Y h:i:s A', $v['date_comment']);?></p>
          <p><?php echo $v['comment'];?></p>
        </div>
        <div class="col-md-2 user">
          <p><?php echo $v['user_name'];?></p>
          <p><?php echo $v['user_city'];?></p>
        </div>
      <?php endif;?>
    </div>
  <?php endforeach;?>
</div>

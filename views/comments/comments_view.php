<div class="row">
  <div class="col-md-offset-3 col-md-6 text-center">
    <h3>New comment</h3>
    <form action="comment" method="post">
      <div class="form-group">
        <label class="sr-only" for="comment">Comment</label>
        <textarea class="form-control" name="comment" id="" cols="100" rows="5" placeholder="Add comment..."></textarea>
      </div>
      <div class="form-group">
        <img src="captcha.php" alt="">
      </div>
      <div class="form-group col-md-offset-3 col-md-6">
        <label class="sr-only" for="captcha">Captcha</label>
        <input type="text" name="captcha" placeholder="Enter captcha" class="form-control">
      </div>
      <div class="text-center">
        <input type="submit" name="add_comment" class="btn btn-default" value="Add comment!">
      </div>
    </form>
  </div>
</div>



<div class="row">
  <div class="col-md-12 text-center">
    <h2>Weather in <?= $data['name'];?> </h2>
    <div class="col-md-offset-3 col-md-6">
      <p class="temp"><img src="<?= 'img/'.$data['weather'][0]->icon.'.png';?>" alt="<?= $data['weather'][0]->description;?>"> <?= round((-273.15+$data['main']->temp),2);?>&ordm;C</p>
      <p><?= $data['weather'][0]->description;?></p>
      <p>Wind: <?= $data['wind']->speed;?> m/s</p>
      <p>Pressure: <?= $data['main']->pressure;?> hPa</p>
      <p>Humidity: <?= $data['main']->humidity;?> &#37;</p>
      <p>Sunrise: <?= date('H:i', $data['sys']->sunrise);?></p>
      <p>Sunset: <?= date('H:i', $data['sys']->sunset);?></p>
    </div>
  </div>
</div>
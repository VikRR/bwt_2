<?php

/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 28.12.16
 * Time: 22:19
 */

class Comment_model extends Model
{

  /**
   * Comment_model constructor.
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * @param array $data
   * @return bool
   */
  public function setComment($data){
    $ps = $this->database();
    try{
      $ins = $ps -> prepare('insert into comments (user_id, comment, date_comment) values(?,?,?)');
      $ins -> execute($data);
      return true;
    }catch(PDOException $e){
      echo '<br></strong> Insert failed. ' . $e -> getMessage() . '</strong>';
      return false;
    }
  }

}
<?php

/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 28.12.16
 * Time: 22:19
 */
use GuzzleHttp\Client;

class Weather_model extends Model
{

  /**
   * @return object
   */
  public function getWeather(){
    $client = new Client([
        'base_uri'=>'http://api.openweathermap.org/',
        'timeout'=> 2.0
        ]);
    $request = $client->request('GET', 'data/2.5/weather?id=687700&appid=9c003d5b308ab0437f81956d70c4927d');
    $weather =$request->getBody();
    return $weather = $weather->getContents();
  }
}
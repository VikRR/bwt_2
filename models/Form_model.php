<?php

/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 28.12.16
 * Time: 22:18
 */
class Form_model extends Model
{
  /**
   * @param string $data
   * @return bool|array mixed
   */
  public function emailVal($data){
    $ps = $this -> database();
    try{
      $sel = $ps -> prepare("select * from users where email=?");
      $sel -> execute(array($data));
      while($row = $sel -> fetch()){
        $result_array = $row;
      }
      return $result_array;
    }catch(PDOException $e){
      echo '<br><strong>Login request failed: ' . $e -> getMessage() . '</strong>';
      return false;
    }
  }

  /**
   * @param array $data
   * @return bool
   */
  public function registration($data){
    $ps = $this -> database();
    try{
      $ins = $ps -> prepare("insert into users (name, last_name, email, password, sex, birthday, city, regdate) values(?,?,?,?,?,?,?,?)");
      $ins -> execute($data);
      return true;
    }catch(PDOException $e){
      echo '<br><strong>Insert request failed: ' . $e -> getMessage() . '</strong>';
      return false;
    }
  }

  /**
   * @param string $name
   * @param string $last_name
   * @return bool|array mixed
   */
  public function nameVal($name, $last_name){
    $ps = $this -> database();
    try{
      $sel = $ps -> prepare("select * from users where name = ? and last_name = ? ");
      $sel -> execute(array($name,$last_name));
      while($row = $sel -> fetch()){
        $result_array=$row;
      }
      return $result_array;
    }catch(PDOException $e){
      echo '<br><strong>Email request failed: ' . $e -> getMessage() . '</strong>';
      return false;
    }
  }
}